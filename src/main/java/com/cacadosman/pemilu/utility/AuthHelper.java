package com.cacadosman.pemilu.utility;

import com.cacadosman.pemilu.model.User;
import com.cacadosman.pemilu.repository.UserRepository;
import com.cacadosman.pemilu.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthHelper {

    @Autowired
    UserService userService;

    public boolean isLoggedIn(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.isAuthenticated() && !(auth instanceof AnonymousAuthenticationToken);
    }

    public User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(isLoggedIn()){
            return userService.findByUsername(auth.getName());
        }
        return null;
    }
}
