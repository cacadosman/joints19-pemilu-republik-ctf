package com.cacadosman.pemilu.utility;

import org.springframework.stereotype.Component;

@Component
public class QueryFilter {

    public String parse(String sentence){
        return sentence
                .replaceAll(" ", "")
                .replaceAll("\"", "")
                .replaceAll("(?i)select", "")
                .replaceAll("(?i)from", "")
                .replaceAll("(?i)where", "");
    }
}
