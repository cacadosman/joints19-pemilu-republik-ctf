package com.cacadosman.pemilu.utility;

import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class ResponseHelper<T> {

    private boolean status = true;

    public void setStatus(boolean status){
        this.status = status;
    }

    public HashMap sendSimpleResponse(String message){
        HashMap results = new HashMap();
        results.put("status", this.status);
        results.put("message", message);
        return results;
    }

    public HashMap sendData(T data){
        HashMap results = new HashMap();
        results.put("status", this.status);
        results.put("data", data);
        return results;
    }
}
