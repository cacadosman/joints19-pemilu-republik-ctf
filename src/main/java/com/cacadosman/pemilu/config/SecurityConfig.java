package com.cacadosman.pemilu.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;


@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private CustomAuthenticationProvider authProvider;
 
	 @Override
	 protected void configure(HttpSecurity http) throws Exception {
	 	http.authorizeRequests()
				.antMatchers("/", "/login", "/auth" , "/register").permitAll()
	 			.anyRequest().authenticated().and().formLogin().loginPage("/login")
				.successHandler(authenticationSuccessHandler())
				.failureHandler(authenticationFailureHandler())
				.and()
				.logout().permitAll()
				.and().logout()
				.logoutUrl("/logout")
				.logoutSuccessUrl("/").and()
				.csrf().disable();

	 }

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider);
	}


	public AuthenticationSuccessHandler authenticationSuccessHandler(){
	 	return ((request, response, authentication) -> {
	 		response.sendRedirect("/dashboard");
		});
	}

	public AuthenticationFailureHandler authenticationFailureHandler(){
	 	return ((request, response, exception) -> {
			response.sendRedirect("/login?error=1");
		});
	}
}