package com.cacadosman.pemilu.repository;

import com.cacadosman.pemilu.model.Result;
import com.cacadosman.pemilu.model.User;
import com.cacadosman.pemilu.service.DataSourceService;
import com.cacadosman.pemilu.service.UserService;
import com.cacadosman.pemilu.utility.AuthHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ResultRepository {

    @Autowired
    AuthHelper authHelper;

    @Autowired
    UserService userService;

    private Connection connection;

    public ResultRepository(){
        connection = DataSourceService.getInstance().getConnection();
    }

    public Result resultWrapper(ResultSet resultSet) throws SQLException{
        Result result = null;
        if(!resultSet.first())
            return null;
        resultSet.previous();
        while(resultSet.next()){
            int userId = resultSet.getInt("user_id");
            User user = userService.find(userId);
            result = Result.builder()
                    .id(Integer.parseInt(resultSet.getString("id")))
                    .user(user)
                    .value(resultSet.getString("value"))
                    .build();
        }

        return result;
    }

    public Result getLast(){
        String query = "SELECT * FROM results r INNER JOIN users u WHERE u.id=r.user_id and u.id=? ORDER BY r.id DESC LIMIT 1";
        Object[] queryArgs = new Object[]{authHelper.getCurrentUser().getId()};
        ResultSet resultSet;
        try{
            resultSet = DataSourceService.fetchAll(query, queryArgs);
            return resultWrapper(resultSet);
        }catch(SQLException e){
            return null;
        }
    }

    public String getVoteCount(){
        String query = "SELECT count(1) AS total FROM results r INNER JOIN users u WHERE u.id=r.user_id and u.id=?";
        Object[] queryArgs = new Object[]{authHelper.getCurrentUser().getId()};
        ResultSet resultSet;
        try{
            resultSet = DataSourceService.fetchAll(query, queryArgs);
            resultSet.next();
            return resultSet.getString("total");
        }catch (SQLException e){
            return null;
        }
    }

    public boolean vote(String value){
        String query =  String.format("INSERT INTO results (value, user_id) VALUES (%s, %s)", value, "?");
        Object[] queryArgs = new Object[]{authHelper.getCurrentUser().getId()};
        boolean result = false;
        try{
            result = DataSourceService.execute(query, queryArgs);
        }catch (SQLException e){
        }
        return result;
    }
}
