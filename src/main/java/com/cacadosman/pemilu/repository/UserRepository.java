package com.cacadosman.pemilu.repository;

import com.cacadosman.pemilu.model.User;
import com.cacadosman.pemilu.service.DataSourceService;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserRepository {

    private Connection connection;

    public UserRepository(){
        connection = DataSourceService.getInstance().getConnection();
    }

    private User userWrapper(ResultSet resultSet) throws SQLException{
        User user = null;
        if(!resultSet.first())
            return null;
        resultSet.previous();
        while(resultSet.next()){
            user = User.builder()
                    .id(Integer.parseInt(resultSet.getString("id")))
                    .username(resultSet.getString("username"))
                    .password(resultSet.getString("password"))
                    .role(resultSet.getString("role"))
                    .build();
        }

        return user;
    }

    public User findByUsername(String username){
        String query = "SELECT * FROM users WHERE username= ? limit 1";
        Object[] queryArgs = new Object[]{username};
        ResultSet resultSet;
        User user = null;
        try{
            resultSet = DataSourceService.fetchAll(query, queryArgs);
            user = userWrapper(resultSet);
        }catch (SQLException e){
            return null;
        }

        return user;
    }

    public User find(int id){
        String query = "SELECT * FROM users WHERE id=? limit 1";
        Object[] queryArgs = new Object[]{id};
        ResultSet resultSet;
        User user = null;
        try{
            resultSet = DataSourceService.fetchAll(query, queryArgs);
            user = userWrapper(resultSet);
        }catch(SQLException e){
            return null;
        }

        return user;
    }

    public boolean insert(User user){
        String query = "INSERT INTO users (username, password, role) VALUES (?, ?, ?)";
        Object[] queryArgs = new Object[]{user.getUsername(), user.getPassword(), user.getRole()};
        boolean result = false;
        try{
            result = DataSourceService.execute(query, queryArgs);
        }catch (SQLException e){
        }
        return result;
    }
}
