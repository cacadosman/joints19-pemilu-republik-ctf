package com.cacadosman.pemilu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

@Controller
public class ViewController {

    @GetMapping({"/"})
    public String index(Model model){
        return "index";
    }

    @GetMapping("/login")
    public ModelAndView login(Model model, @RequestParam(value="error", defaultValue = "0") String error){
        model.addAttribute("error", error);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(model);
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @GetMapping("/register")
    public String register(Model model){
        return "register";
    }

    @GetMapping("/dashboard")
    public String dashboard(Model model){
        return "vote";
    }

    // VIEW CONTROLLER YANG LAIN BELUM DIBIKIN,
    // PROBSETNYA MAGER, LIAT KODE FRONTNYA DI FOLDER resources/templates AJA YA :v
}
