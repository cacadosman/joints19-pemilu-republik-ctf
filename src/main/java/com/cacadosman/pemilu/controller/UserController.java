package com.cacadosman.pemilu.controller;


import com.cacadosman.pemilu.model.User;
import com.cacadosman.pemilu.service.UserService;
import com.cacadosman.pemilu.utility.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    ResponseHelper responseHelper;

    /**
     * Register User
     * @param user (Json)
     * @return Json
     */
    @PostMapping("/register")
    public HashMap register(@RequestBody User user){
        String result = userService.register(user.getUsername(), user.getPassword());
        responseHelper.setStatus(true);
        return responseHelper.sendSimpleResponse(result);
    }

    @GetMapping("/auth")
    public HashMap authResult(@RequestParam(name = "success", defaultValue = "0") int success){
        String message = "Login Gagal";
        boolean status = false;
        if(success == 1){
            message = "Login Sukses";
            status = true;
        }

        responseHelper.setStatus(status);
        return responseHelper.sendSimpleResponse(message);
    }

    @GetMapping("/profile")
    public HashMap profile(){
        responseHelper.setStatus(true);
        return responseHelper.sendData(userService.getProfile());
    }
}
