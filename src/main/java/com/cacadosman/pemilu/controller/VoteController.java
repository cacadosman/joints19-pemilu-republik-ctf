package com.cacadosman.pemilu.controller;

import com.cacadosman.pemilu.model.wrapper.ResultWrapper;
import com.cacadosman.pemilu.service.ResultService;
import com.cacadosman.pemilu.utility.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class VoteController {

    @Autowired
    ResultService resultService;
    @Autowired
    ResponseHelper responseHelper;

    @GetMapping("/my-last-vote")
    public HashMap getLastVote(){
        String myVote = resultService.getLast();
        String message = String.format("Terakhir kali, anda memilih paslon bernomor urut %s", myVote);

        responseHelper.setStatus(true);
        return responseHelper.sendSimpleResponse(message);
    }

    @GetMapping("/my-vote-count")
    public HashMap getVoteCount(){
        String voteCount = resultService.getVoteCount();
        String message = String.format("Anda telah memilih sebanyak %s kali", voteCount);

        responseHelper.setStatus(true);
        return responseHelper.sendSimpleResponse(message);
    }

    @PostMapping("/vote")
    public HashMap vote(@RequestBody ResultWrapper resultWrapper){
        String value = resultWrapper.getValue();

        resultService.vote(value);

        responseHelper.setStatus(true);
        return responseHelper.sendSimpleResponse("Terima kasih telah memilih :)");
    }

}
