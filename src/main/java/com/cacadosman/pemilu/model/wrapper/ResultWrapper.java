package com.cacadosman.pemilu.model.wrapper;

import lombok.Data;

@Data
public class ResultWrapper {
    private String value;
}
