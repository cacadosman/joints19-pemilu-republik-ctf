package com.cacadosman.pemilu.service;

import com.cacadosman.pemilu.model.Result;
import com.cacadosman.pemilu.model.User;

public interface ResultService {
    public String getLast();
    public String getVoteCount();
    public boolean vote(String value);
}
