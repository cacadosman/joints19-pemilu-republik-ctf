package com.cacadosman.pemilu.service;

import com.cacadosman.pemilu.model.Result;
import com.cacadosman.pemilu.model.User;
import com.cacadosman.pemilu.repository.ResultRepository;
import com.cacadosman.pemilu.repository.UserRepository;
import com.cacadosman.pemilu.utility.QueryFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("resultService")
public class ResultServiceImpl implements ResultService {

    @Autowired
    ResultRepository resultRepository;
    @Autowired
    QueryFilter queryFilter;

    public String getLast(){
        Result result = resultRepository.getLast();
        if(result == null){
            return null;
        }

        return result.getValue();
    }

    public String getVoteCount(){
        return resultRepository.getVoteCount();
    }

    public boolean vote(String value){
        return resultRepository.vote(queryFilter.parse(value));
    }
}
