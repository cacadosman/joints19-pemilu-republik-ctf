package com.cacadosman.pemilu.service;

import com.cacadosman.pemilu.model.User;

public interface UserService {
    public User findByUsername(String username);
    public User find(int id);
    public String register(String username, String password);
    public User getProfile();
}
