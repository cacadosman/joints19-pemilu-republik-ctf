package com.cacadosman.pemilu.service;

import com.cacadosman.pemilu.model.User;
import com.cacadosman.pemilu.repository.UserRepository;
import com.cacadosman.pemilu.utility.AuthHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    AuthHelper authHelper;

    public User findByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public User find(int id){
        return userRepository.find(id);
    }

    public String register(String username, String password){
        if(userRepository.findByUsername(username) != null)
            return "Username sudah terdaftar";
        User user = User.builder()
                .username(username)
                .password(password)
                .role("ROLE_USER")
                .build();
        boolean result = userRepository.insert(user);
        if(!result)
            return "Registrasi Gagal";
        return "Registrasi Sukses";
    }

    public User getProfile(){
        User auth = authHelper.getCurrentUser();
        return auth;
    }
}
