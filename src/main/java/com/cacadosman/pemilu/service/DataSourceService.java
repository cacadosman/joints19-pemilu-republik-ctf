package com.cacadosman.pemilu.service;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class DataSourceService {
        @Autowired
    private Environment env;

    private static DataSourceService INSTANCE = null;

    private static Connection connection =  null;

    private BasicDataSource dataSource;

    private static final String DATABASE_URL = "jdbc:mysql://mysql-app:3306/pemiluctf";

    private static final String DATABASE_USERNAME = "root";

    private static final String DATABASE_PASSWORD = "joints2019";

    private static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";

    public DataSourceService(){
        dataSource = new BasicDataSource();
        dataSource.setDriverClassName(DATABASE_DRIVER);
        dataSource.setUrl(DATABASE_URL);
        dataSource.setUsername(DATABASE_USERNAME);
        dataSource.setPassword(DATABASE_PASSWORD);
        try{
            connection = dataSource.getConnection();
        }catch (SQLException e){
            System.out.println("error connecting to database");
            e.printStackTrace();
        }
    }

    public static DataSourceService getInstance(){
        if(INSTANCE == null){
            return new DataSourceService();
        }
        return INSTANCE;
    }

    public Connection getConnection(){
        return connection;
    }

    private static PreparedStatement statementParser
            (PreparedStatement statement, Object[] args) throws SQLException{

        if(args == null)
            return statement;

        int count = 0;
        for(Object arg : args){
            count++;
            if(arg instanceof String)
                statement.setString(count, arg.toString());
            else if(arg instanceof Integer)
                statement.setInt(count, Integer.parseInt(arg.toString()));
            else if(arg instanceof Double)
                statement.setDouble(count, Double.parseDouble(arg.toString()));
            else if(arg instanceof Float)
                statement.setFloat(count, Float.parseFloat(arg.toString()));
        }
        return statement;
    }

    public static boolean execute(String sql, Object[] args) throws SQLException{
        PreparedStatement statement = connection.prepareStatement(sql);
        statement = statementParser(statement, args);
        return statement.executeUpdate() == 1;
    }

    public static ResultSet fetchAll(String sql, Object[] args) throws SQLException{
        PreparedStatement statement = connection.prepareStatement(sql);
        statement = statementParser(statement, args);
        return statement.executeQuery();
    }

}
