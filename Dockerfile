FROM openjdk:8-jre

ADD ./target/pemilu-0.1.0.jar pemilu.jar
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /pemilu.jar